package com.jretuerto.bootcamp.ui

import android.content.Intent
import android.os.Bundle
import com.jretuerto.bootcamp.R
import com.jretuerto.bootcamp.databinding.ActivitySimuladorBinding
import dagger.hilt.android.AndroidEntryPoint
import pe.com.bootcamp.jretuerto.data.entities.simulador.SimuladorResponse

@AndroidEntryPoint
class SimuladorActivity : BaseActivity() {
    private lateinit var binding: ActivitySimuladorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySimuladorBinding.inflate(layoutInflater)
        setContentView(binding.root, R.id.claSimulador)

        initializeUI()

        binding.regresarButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    fun initializeUI() {

        intent.getParcelableExtra<SimuladorResponse>("value")?.let { response ->

            val strs = response.response.primeraCuota.split("-").toTypedArray()

            binding.cuotaTextView.text = "S/ " + response.response.cuota
            binding.fechaPagoTextView.text = strs[2] + "/" + strs[1] + "/" + strs[0]
        }

    }
}
