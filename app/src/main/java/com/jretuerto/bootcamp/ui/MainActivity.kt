package com.jretuerto.bootcamp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.jretuerto.bootcamp.R
import com.jretuerto.bootcamp.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import pe.com.bootcamp.jretuerto.data.entities.listas.ListasResponse
import pe.com.bootcamp.jretuerto.data.entities.simulador.SimuladorResponse
import pe.com.bootcamp.jretuerto.viewmodel.BCPViewModel
import java.text.Normalizer

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding

    private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()
    private val viewModel: BCPViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root, R.id.claMain)

        viewModel.doListasSimulador()

        binding.enviarConsultaButton.setOnClickListener {

            if (!isEditTextEmpty(binding.dniEditText) && !isEditTextEmpty(binding.montoEditText)) {
                viewModel.doSimulador(fillFormRequest())
            }
        }

        setupViewModel()

    }

    private fun setupViewModel() {
        viewModel.isViewLoading.observe(this, isViewLoadingObserver)
        viewModel.listaSimulador.observe(this, listasSimuladorObserver)
        viewModel.simulador.observe(this, simuladorObserver)
        viewModel.onMessageError.observe(this, onMessageErrorObserver)
    }

    private val listasSimuladorObserver = Observer<ListasResponse> { response ->

        // Tipos de tarjeta
        var tiposTarjeta = arrayOf<String>(
            response.response.tarjetas.clasica,
            response.response.tarjetas.oro,
            response.response.tarjetas.black
        )
        var adapterTipoTarjeta= ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            tiposTarjeta
        )
        binding.tipoTarjetaSpinner.adapter = adapterTipoTarjeta

        // Cuota
        var adapterCuota= ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            response.response.cuotas
        )
        binding.cuotasSpinner.adapter = adapterCuota

        // Tasa
        var adapterTasa= ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            response.response.tea
        )
        binding.teaSpinner.adapter = adapterTasa

        // Dia de pago
        var adapterDiaPago= ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            response.response.dias_pagos
        )
        binding.diaPagoSpinner.adapter = adapterDiaPago
    }

    private val simuladorObserver = Observer<SimuladorResponse> { response ->

        if (response.code.equals("200")) {
            val intent = Intent(this, SimuladorActivity::class.java)
            intent.putExtra("value", response)
            startActivity(intent)
        } else {
            Toast.makeText(this, "Error: ${response.mensaje}", Toast.LENGTH_SHORT).show()
        }
    }

    private val isViewLoadingObserver = Observer<Boolean> {

        if (it) {
            this.showLoadingView("Cargando")
        } else {
            this.hideLoadingView()
        }

    }

    private val onMessageErrorObserver = Observer<String> {

        showDialog(errorBody = it)
    }

    private fun isEditTextEmpty(editText: EditText) : Boolean {

        var texto = editText.text.toString().trim()

        if (TextUtils.isEmpty(texto)){
            editText.setError("Campo requerido")
            editText.requestFocus()
            return true
        } else {
            return false
        }
    }

    private fun fillFormRequest(): HashMap<String?, String?>{
        val params = HashMap<String?, String?>()
        params["dni"] = binding.dniEditText.text.toString()
        params["tarjeta"] = binding.tipoTarjetaSpinner.selectedItem.toString().toLowerCase().unaccent()
        params["monto"] = binding.montoEditText.text.toString()
        params["cuotas"] = binding.cuotasSpinner.selectedItem.toString()
        params["tea"] = binding.teaSpinner.selectedItem.toString()
        params["dia_pago"] = binding.diaPagoSpinner.selectedItem.toString()

        return params
    }

    fun CharSequence.unaccent(): String {
        val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
        return REGEX_UNACCENT.replace(temp, "")
    }

}