package pe.com.bootcamp.jretuerto.viewmodel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import pe.com.bootcamp.jretuerto.data.entities.listas.ListasResponse
import pe.com.bootcamp.jretuerto.data.entities.simulador.SimuladorResponse
import pe.com.bootcamp.jretuerto.data.repository.BCPRepository
import pe.com.bootcamp.jretuerto.data.remote.Result
import javax.inject.Inject

@HiltViewModel
class BCPViewModel @Inject constructor(private val repository: BCPRepository) :
    BaseViewModel() {

    private val _listaSimulador = MutableLiveData<ListasResponse>()
    val listaSimulador: LiveData<ListasResponse> = _listaSimulador

    private val _simulador = MutableLiveData<SimuladorResponse>()
    val simulador: LiveData<SimuladorResponse> = _simulador

    protected val _isViewLoading = MutableLiveData<Boolean>()
    val isViewLoading: LiveData<Boolean> = _isViewLoading

    protected val _onMessageError = MutableLiveData<String>()
    val onMessageError: LiveData<String> = _onMessageError


    fun doListasSimulador() {
        viewModelScope.launch {

            _isViewLoading.postValue(true)

            val result: Result<ListasResponse> = withContext(Dispatchers.IO) {
                repository.listasSimulador()
            }

            when (result) {
                is Result.Success -> {
                    _listaSimulador.value = result.data
                }

                is Result.ApiError -> {
                    _onMessageError.postValue("Hubo un problema de conexión")

                }
            }

            _isViewLoading.postValue(false)
        }
    }

    fun doSimulador(request: HashMap<String?, String?>) {
        viewModelScope.launch {

            _isViewLoading.postValue(true)

            val result: Result<SimuladorResponse> = withContext(Dispatchers.IO) {
                repository.postSimulador(request)
            }

            when (result) {
                is Result.Success -> {
                    _simulador.value = result.data
                }

                is Result.ApiError -> {
                    _onMessageError.postValue("Hubo un problema de conexión")

                }
            }

            _isViewLoading.postValue(false)
        }
    }


}