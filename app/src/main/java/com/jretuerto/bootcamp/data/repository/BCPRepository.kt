package pe.com.bootcamp.jretuerto.data.repository

import pe.com.bootcamp.jretuerto.data.entities.listas.ListasResponse
import pe.com.bootcamp.jretuerto.data.entities.simulador.SimuladorResponse
import pe.com.bootcamp.jretuerto.data.remote.BCPRemoteDataSource
import pe.com.bootcamp.jretuerto.data.remote.Result

import javax.inject.Inject

class BCPRepository @Inject constructor(
    private val remoteDataSource: BCPRemoteDataSource
) {

    suspend fun listasSimulador(): Result<ListasResponse> = remoteDataSource.listasSimulador()

    suspend fun postSimulador(request: HashMap<String?, String?>): Result<SimuladorResponse> = remoteDataSource.postSimulador(request)


}


