package pe.com.bootcamp.jretuerto.data.entities.listas

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Response(
    val tarjetas: Tarjeta,
    val cuotas: List<Int>,
    val dias_pagos: List<String>,
    val tea: List<String>
) : Parcelable