package pe.com.bootcamp.jretuerto.data.entities.listas

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ListasResponse(
    val code: Int,
    val response: Response

) : Parcelable

