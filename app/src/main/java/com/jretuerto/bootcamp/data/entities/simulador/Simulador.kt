package pe.com.bootcamp.jretuerto.data.entities.simulador

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Simulador(
    val cuota: String,
    val moneda: String,
    val primeraCuota: String,
    val estado: String
) : Parcelable