package pe.com.bootcamp.jretuerto.data.entities.listas

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Tarjeta(
    val clasica: String,
    val oro: String,
    val black: String

) : Parcelable