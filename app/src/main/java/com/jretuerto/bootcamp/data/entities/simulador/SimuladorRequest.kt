package pe.com.bootcamp.jretuerto.data.entities.simulador

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SimuladorRequest(
    val dni: String,
    val tarjeta: String,
    val monto: String,
    val cuotas: String,
    val tea: String,
    val dia_pago: String
) : Parcelable

