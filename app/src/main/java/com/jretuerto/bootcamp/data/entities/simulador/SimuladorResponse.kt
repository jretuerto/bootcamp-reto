package pe.com.bootcamp.jretuerto.data.entities.simulador

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SimuladorResponse(
    val code: String,
    val response: Simulador,
    val mensaje: String?
) : Parcelable