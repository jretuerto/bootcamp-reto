package pe.com.bootcamp.jretuerto.data.remote

import com.jretuerto.bootcamp.rest.ApiService
import javax.inject.Inject

class BCPRemoteDataSource @Inject constructor(
    private val service: ApiService
) : BaseDataSource() {


    suspend fun listasSimulador() = safeApiCall { service.listasSimulador() }

    suspend fun postSimulador(request: HashMap<String?, String?>) = safeApiCall { service.postSimulador(request) }





}


