package com.jretuerto.bootcamp.rest

import pe.com.bootcamp.jretuerto.data.entities.listas.ListasResponse
import pe.com.bootcamp.jretuerto.data.entities.simulador.SimuladorResponse

import retrofit2.Response
import retrofit2.http.*


interface ApiService {

    @GET("simulator")
    suspend fun listasSimulador(): Response<ListasResponse>

    @FormUrlEncoded
    @POST("simulator")
    suspend fun postSimulador(@FieldMap request: HashMap<String?, String?>): Response<SimuladorResponse>


}